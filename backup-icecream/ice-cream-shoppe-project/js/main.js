(function () {
    "use strict"
    function renderIceCream(iceCream) {
        var html = '<div class="col-6 m-0">';
        html += '<span class="iceCreamName font-weight-bold text-capitalize">' + iceCream.name + " " + '</span>';
        html += '<span class="iceCreamFlavor font-weight-bold text-black-50 text-capitalize">' + iceCream.flavor + '</span>';
        html += '</div>';
        return html;
    }
    function renderIceCreams(iceCreams) {
        var html = '';
        html = "<div class='row'>";
        for(var i = 0; i < iceCreams.length ; i++) {
            html += renderIceCream(iceCreams[i]);
        }
        html += "</div>";
        return html;
    }
    function updateIceCreams(e) {
        e.preventDefault(); // don't submit the form, we just want to update the data
        var selectedType = typeSelection.value;
        var filteredIceCreams = [];
        iceCreams.forEach(function(iceCream) {
            if (iceCream.roast === selectedType) {
                filteredIceCreams.push(iceCream);
            }
        });
        tbody.innerHTML = renderIceCreams(filteredIceCreams);
    }
    var iceCreams = [
        {id: 1, name: 'Karen', flavor: 'vanilla'},
        {id: 2, name: 'French', flavor: 'vanilla'},
        {id: 3, name: 'Italian', flavor: 'vanilla'},
        {id: 4, name: 'The Dracula', flavor: 'chocolate'},
        {id: 5, name: 'American', flavor: 'chocolate'},
        {id: 6, name: 'Sweet Caroline', flavor: 'strawberry'},
        {id: 7, name: 'Short Cakes', flavor: 'strawberry'},
        {id: 8, name: 'CC Continental', flavor: 'cookies n\' cream'},
        {id: 9, name: 'Cookie Monster', flavor: 'cookies n\' cream'},
        {id: 10, name: 'Oreos W/ Milk', flavor: 'cookies n\' cream'},
        {id: 11, name: 'The Railroad', flavor: 'rocky road'},
        {id: 12, name: 'Rock N\' Roll', flavor: 'rocky road'},
        {id: 13, name: 'The Elvis', flavor: 'rocky road'},
        {id: 14, name: 'MushMellow', flavor: 'rocky road'},
    ];
    var tbody = document.querySelector('#iceCreams');
    var submitButton = document.querySelector('#submit');
    var typeSelection = document.querySelector('#type-selection');
    tbody.innerHTML = renderIceCreams(iceCreams);
    submitButton.addEventListener('click', updateIceCreams);
})();
# Today we will be working on updating Ice-Cream Shoppe’s Website!

## INSTRUCTIONS
1. Fork this repository
2. Clone the forked repository.
3. Read through the existing code that is already on here. 
4. Make sure you go through all the files!


## TODO

* The names of the Ice-creams should be sorted by their ID in ascending order 
when the page loads. 
* You will need to add an input field to the existing form to add 
functionality, so that we are able to search through the ice-cream names.
You will also need to display the names that match the search term. 
* HTML looks outdated, update it!
*  You will need to refactor the tables so that they are in a 'div' as table are outdated. 
Make sure a heading displays the ice-cream name with the types of flavor. 
The ID's should not be displayed. 
* Have the user be able to add new ice-creams to the page
* Create a new form on the page that will take an input for the ice-cream name
and add a select to be able to choose the type of flavor each name will have.
Once the page is submitted, the page should now show the new ice-cream.
(*Extra note/Bonus. When you refresh the page, any ice-creams that have been added will no longer be 
displayed on the page. To help out with this-research `localStorage`)





    


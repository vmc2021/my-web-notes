// //external java notes
// /* this
// is
// a
// test
//  */
// console.log(5 * 100);
// //Data Types
// //numbers,string, boolen, null
//
// //variables
// // let, const, var(most common used)
//
// //Declaring Variables
// // syntax/structure: var nameOFTheVariable;
// // syntax/structure: var nameOfVariable = value;
//
// // Declare a variable named: firstName
// var firstName;
// //Assign the Value of "Billy" to firstName
// firstName = "Billy";
// //CALLED THE VARIABLE NAME not by the value
// console.log(firstName); //refresh and check chrome
// //second way second example: variable name is age value of 30 To age
// // var age = 30;
// console.log(age);
// //OPERATORS
// //MATHAMATICAL ARITHMATIC
// console.log(12/4);
//
// console.log(12 % 5);
// //mixed operations
// console.log( (1+2)*4/5);
// //PEMDAS
//
// //logical operators
//
// /*
// AND &&
// OR ||
// NOT !
//
// NOTE: returns a boolean value when used with boolean values
// also used with non-boolean values
// AND &&
// TRUE STATEMENT && TRUE STATEMENT IS (true)
// TRUE STATEMENT && FALSE STATEMENT is (false)
// FALSE STATEMENT && TRUE STATEMENT IS (FALSE)
// F && F IS WHAT(false)
//  */
// console.log(true && true); // true
// console.log(true && false); //false
// console.log(false && false); //false
//
// //NEXT OPERATOR: OR ||
// //T || F? its (true)
// console.log(true || false); //True
// // T || T ? (true)
// console.log(true || true); // True
// console.log(flase || false); //false
// console.log(false|| true); //True
// // Not !
// // the opposite
// // !true=false
// // !false= true
// console.log(!false); //true
// console.log(!!!!!!!true); //false because of 7!
// // COMPARISON OPERATOR
// // ==,===, <,>, <=,>=
// // the = is used to assign valus(s) to a variable
// // == checks if VALUE IS THE same
// // the === checks if the Value and data type are the same
// var firstPet= "Dog";
// var secondPet= "dog"
//
// console.log(firstPet === secondPet); // false
// //better example of == vs ===
// var entry = 12;
// var entry2 = "12";
// console.log(entry == entry2); // true
// console.log(entry === entry2) // false
//
// // != operator, and the !===
// // != checks if value is not the same
// // !=== checks if the Value and Data Type are Not the same
// console.log(entry != entry2); // false
// console.log(entry !== entry2); // true
//
// // <, >, <=, >=
// const age =21;
// console.log(age >= 21 || age < 18); // true
// //              T     ||  F         = true
// console.log(age <= 21 || age > 18);// true
// //                 t   ||  t
//
// console.log( age < 21 && age > 18); // false
// //             f      &&   T       // false

//CONCATENATION
const person = "bruce"
const person_age = 40;


console.log("Hello my name is " + person);
//next has two strings
console.log("And I am" + person_age + "years old");
// Put them together
console.log("Hello my name is" + person + "and i am" + person_age + "years old")

//TEMPLATE STRINGS `back ticks`
// line 149
console.log(`Hello my name is ${person}`);
console.log(`And I am ${person_age} years old`)
// line 152 153 below
console.log(`Hello my name is ${person} and I am ${person_age} years old.`);

//STRING METHODS...HELP WITH STRING VARIABLES
//.lenght
//gets lenght of string
console.log(person.length); //5 characters
const greeting= "Hello World";
console.log(greeting.length); // 12

// .toUpperCase() / .toLowerCase() Methods
// GET THE STRING IN UPPERCASE/LOWERCASE
console.log(greeting.toUpperCase()); //HELLO WORLD!
console.log(greeting.toLowerCase()); //hello world
//.substring() method
//extracts character from a string between a "start" and "end"
//index- not including the "end" itself

var goat = "Selena Quintanilla-Perez";
console.log(goat.substring(0, 6));
//Quintanilla?
console.log(goat.substring(7, 18));
 //Functions
//Syntax For Function
//function nameOfTheFunction() {put code here}

function sayHello() {
    // console.log("Hello!");
    //or you want an alert below
    alert("hello");
}
//CAll A Function/Activate  
//when we want it to run, we call it by
//its name with the ()
sayHello();
//creat a function w/ parameters like (num1){body} We got NAN not a number because we didnt give num1 a value.
function addNumber(num1) {
    console.log(num1 + 2)
}
//Call it
addNumber(13);
//creat a Function w/ two parameters using Template Strings
function  message(name, age){
    alert(`Hello my name is ${name} and I am ${age} years old.`);
}
//call it
message("Jose", 21)
//Function with Three parametrers using Cancatnation. Add a space is function after last word or period inside quotations
function petInfo(name, animal, age) {
    console.log("My pet's name is " + name + ". " + "He ia a " + animal + ", and he is " + age + "." );
}

petInfo("Chico", "Bulldog", 7)

//Conditionals Notes


/* jQuery Selectors */
// Complete the following in the a new js file name jquery-selector.js
// NOTE: Y
// // "use strict";
// // - Create content in your HTML
// // - h1: your nameou'll need to create a new html name jquery-selectors.html

// - p: your hometown
// - an unordered list of your hobbies(4)
//
// - Add attributes to your elements, you will need both id and class attributes
// - Use jQuery to select an element by the id. Create an alert with the contents of the element
// - Update the jQuery code to select and alert a different id
// - Use the same id on 2 elements. How does this change the jQuery selection?The order in which an id is called will be first id with name the second id with duplicate name on first will not be called
//     - Remove the duplicate id. Each id should be unique on that page.

// var name = $("#name").html()
// alert(name);
//
// var hometown = $("#hometown").html()
// alert(hometown);
//
// var music = $("#music").html()
// alert(music);
//
// var section1 = $(".section1").html()
// alert(section1)
//
// var section2 = $(".section2").html()
// alert(section2);
//
// var myList = $(".mylist").html()
//     alert(myList);


// Class Selectors
// - Remove/comment out your custom jQuery code from the previous exercises.
// - Update your code so that at least 3 different elements have the same class name 'codebound'
// - Using jQuery, create a border around all elements with the class 'codebound' that is 2 pixels around and red
// - Remove the class from one of the elements. Refresh and test the border has been removed, yes the border has been removed.
// - Give another element an id of 'codebound'. Did this element get a border?No, the element did not receive the border.


// $(".codebound").css("border", "2px solid");

// $("li").css("font-size", "24px");
// //change every li
// $("h1").css("color", "blue")
// $("p").css("color", "red")
// $("li").css("color", "orange")

// Element Selectors
// - Remove/comment out your jQuery code from the previous lesson
// - Using jQuery, set the font-size of all 'li' elements to 24px
// - Change the font colors of all h1, p, and li element (all should be different colors)
// - Create a jQuery statement that alerts the contents of your h1 element(s)

// var head1 = $("#name").html;
//
// alert(head1);
//
// var head2 = $("#country.html;
//
// alert(head2);



// Multiple Selectors
// - Change to font colors and sizes of all h1, p, and li elements to be the same.

    $("h1, p, li").css(
        {
            "color": "blue",
            "font-size": "70px"

        }
    )








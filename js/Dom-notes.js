// get the main header
var mainHeader = document.getElementById('main-header');
console.log(mainHeader); // html structure

console.log(mainHeader.innerHTML); // Hello world!

//set inner html of mainHeader to "Winter is Gone!"
mainHeader.innerHTML = "Winter is Gone!";

// assign a var named 'subHeader', to the sub header element by id

var subHeader = document.getElementById('sub-header');

//changing style. set the text color of subHeader to blue
subHeader.style.color = "blue"; // u can enter hex or actual value inside quotes

// Todo: assign a variable  named
var listItems = document.getElementsByTagName("li");

// todo: set the text color on every other list item to gray
// also, set the text color of li with data-dbid = 1 to blue

for (var i = 0; i < listItems.length; i++) {
    if (i % 2 !== 0) {
        listItems[i].style.color = 'red';
        // add ! to (i % 2 !== 0) to just set 2 and 4 red
    }

    if (listItems[i].hasAttribute('data-dbid')) {

        if (listItems[i].getAttribute('data-dbid') === '1') {
            listItems[i].style.color = 'blue';
        }
    }

}

// change the content
var subParagraph = document.getElementsByClassName("sub-paragraph");
// you can console.log(subParagraph); but not necassary. you get HTMLCollection



//specifically modify a subParagraph
subParagraph.item(0).innerHTML = "This is new content for the sub paragraph";



//how to add this to web page
// taking a string and converting it to an array

var planetsString = "Mercury|Venus|Earth|Mars|Jupiter|Saturn|Uranus|Neptune";
var planetsArray = planetsString.split("|");


//create a var
//taking the planetsArray and putting it in an unordered list
var html = "<ul><li>";

html += planetsArray.join("</li><li>");

html += "</li></ul>";


//taking the unordered list and adding it to thr html elemen t with
//the id of planets
var planetsList = document.getElementById("planets");
planetsList.innerHTML = html;

























































































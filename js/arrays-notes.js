// Arrays ist can hold anything inside
// syntax []
//Example
// var myArray = ["red", 12, -44, true]; // index based, we have a lenght of 4
// console.log(myArray);
//
// console.log(myArray.length); // 4
//
// console.log(myArray.length[2]); // -144
//
// console.log(myArray.length[4]); // undefined

//looping inside array
//iterating array
var delta = ["jose", "alyssa", "victor", "angela"];
//
// console.log("There are " + delta.length + "members in Delta Cohort") // concatenation
// Loop  Through the Array and console log the values
for (var i = 0; i < delta.lenght; i++) {
    //print out all the values for delta...
    // individually
    console.log(delta[i]); // we use i
}
// for Each loop
// syntax:
// maeOFVariable.forEach( function ( elemnt, index, array ) {
//              .. run code
//              });
// example

    //
    var ratings = [25, 34, 57, 62, 78, 89, 91];
    //
    //
    // ratings.forEach(function (rating) {
    // console.log(rating);
    // });

    //example

    var pizza = ["cheese", "dough", "sauce", "pineapple", "canadian bacon" ];
    // pizza.forEach(function (ingredient) {
    //     console.log(ingredient)
    // })

// changing / manipulating arrays
    var fruits = ["apple", "grape", "strawberry", "banana"];
    console.log(fruits)
//add to the array
    fruits.push("watermelon"); // adds to the end of array
console.log(fruits);

fruits.unshift("dragon fruit");
console.log(fruits);

//adding more than one item
fruits.push("cherry", "mango", "pineapple");
console.log(fruits)


//remove from array

// remove the last element of the array..?
// fruits.pop();


//remove the first element of array..?

// fruits.shift();
// console.log(fruits);


//Locating array elements(s)
 var indexElement = fruits.indexOf("strawberry");
// console.log(indexElement); //2

//Slicing
// .slice() - copies a portion of the  original array
var slice = fruits.slice(1, 3);
console.log(slice)
console.log(fruits);

//Reverse
fruits.reverse();
console.log(fruits);

//sort
console.log(fruits.sort()); // using .sort method















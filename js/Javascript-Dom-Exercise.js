/* JAVASCRIPT DOM */
// NOTE: You'll need to create an html to link this js file, along with creating some content in the html
"use strict";
/** When the button with the id of `change-bg-color` is clicked the background of
 the page should turn blue.*/

var button1 = document.getElementById('changeBgColor')
function toggleBackgroundColor() {
    if (document.body.style.backgroundColor === "blue") {
        document.body.style.backgroundColor = "white";
    }
    else {
        document.body.style.backgroundColor = "blue";
    }

}

changeBgColor.addEventListener("click", toggleBackgroundColor);


/** When the button with an id of `append-to-ul` is clicked, append an li with
 the content of `text` to the ul with the id of `append-to-me`.*/

var addBtn = document.getElementById("append-to-ul");

function addLiToList() {

    // create a variable to target the #append-to-me
    var list = document.getElementById("append-to-me");

    // created a variable to create the element 'li' that will be added to the ul
    var li = document.createElement("li");

    // adding content "text" to the li variable
    li.appendChild(document.createTextNode("text"));

    // add the variable 'li' to the ul
    list.appendChild(li);

}


/** Ten seconds after the page loads, the heading with the id of `message` should
 change it's text to "Goodbye, World!". Completed*/

var heading = document.getElementById("message");

setTimeout(function () {
    heading.innerHTML = "Goodbye, World!";
}, 10000);


/** When a list item inside of the ul with the id of `hl-toggle` is first
 clicked, the background of the li that was clicked should change to
 yellow.
 When a list item that has a yellow background is clicked, the
 background should change back to the original background.
 */
// jose's solution
var hlItem = document.getElementById('hl-toggle');

function highlightItem() {
    if (hlItem.style.backgroundColor === 'yellow') {
        hlItem.style.backgroundColor = 'white';
    } else {
        hlItem.style.backgroundColor = 'yellow';
    }
}

hlItem.addEventListener('click', highlightItem);





/** When the button with the id of `upcase-name` is clicked, the element with the
 id of `output` should display the text "Your name uppercased is: " + the
 value of the `input` element with the id of `input` transformed to uppercase.*/

// create a variable and assign it to the element with the id of 'upcase-name'

    
var upCasebtn = document.getElementById("upcase-name");

// create a variable and assign it to the element with the id of 'input'
var inputEl = document.getElementById("input");

// create a variable and assign it to the element with the id of 'output'
var outputEl = document.getElementById("output");

// create a function, name it something that says it is going to convert an input to uppercase

function convertInput() {

    // declare a variable that will capture the value of your inputEl variable, and uppercase it
    var userInput = inputEl.value.toUpperCase();

    // test the userInput variable in a console log
    console.log(userInput);

    // attach a innerHTML to the variable that assigned to the output id, and assign it to the userInput variable
    outputEl.innerHTML = userInput;
}

// attach your variable that is assigned to the upcase-name id, to an click event listener
upCasebtn.addEventListener("click", convertInput)

/** Whenever a list item inside of the ul with the id of `font-grow` is _double_
 clicked, the font size of the list item that was clicked should double.*/

// jose's solution
var growItem = document.getElementById('font-grow');
function doubleSize() {
    growItem.style.fontSize = '200%';
}
growItem.addEventListener('dblclick',doubleSize);





















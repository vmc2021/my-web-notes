//while Loop


//Example


//     var number = 0;
//     while (number <= 100) {
//         console.log(`# ${number}`);
//
//     //increment the value by 3...
//         number += 3;
//
// }
// do-while loop
// example 2---------------

    // var x = 10;
    //
    //
    // do {
    //     //code block
    //     console.log(`do-while loop # ${x}`);
    //     //decrement the vale by 1
    //     // x--; or x -=1
    //     x--;
    // } while ( x>= 0);
// Example 3------------
//-----for loop-------
// for (var x = 100; x <= 200; x += 10) {
//     console.log("for loop # " + x);
// }

//----example 4  break and continue---------
// var endAtNumber = 5; // Global variable
// for (var i = 1; i <= 100; i++) {
//     console.log(`loop count # ${i}`);
//
//     //if statement
//     if (i === endAtNumber) {
//         alert("We have reach our break!");
//         break;
//         console.log("Do you see this message? Do you??");
//     }
//
// }

// for (var n = 1; n <= 100; n++) {
//     // if statement
//     if (n % 2 !== 0) {
//         continue; // continue means skip
//     }
//     console.log("Even Number: " + n);
//
// }

// using a while loop, console log creat the output shown below:
// 2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65536

    // var number = 2;
    // while (number <= 65536) {
    // console.log(number)
    //
    //     number *= 2;
    //     // other shorthand increments / decrements
    //     //number += 2;
    //     //number -= 2;
    //     // number /= 2;
    //
    // }

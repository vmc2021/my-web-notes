/* JAVASCRIPT OBJECTS */
// Complete the following in a new js file named objects-exercise.js
"use strict";
/**
 * TODO: Create an object called movie. Include four properties
 *  Console log each property.
 */
/**
 * TODO: Create an object called movie include four properties of title, protagonist, antagonist, genre
 *  console log each property.
 */
/**
 * TODO: Create a pet/companion object include the following properties:
 *  string name, number age, string species, number valueInDollars,
 *  array nickNames, vaccinations: date, type, description.
 */
/**
 * TODO: Add 3 additional objects to the array below called movies. Be sure to include five properties: title, releaseDate, director, genre, and myReview.
 *  The myReview property should be a function
 */
//EX
// const movies = [
//     {
//         title: 'Batman',
//         releaseDate: {
//             month: June,
//             date: 23,
//             year: 1989
//         },
//         director: {
//             firstName: 'Tim',
//             lastName: 'Burton'
//         },
//         genre: ['Action', 'Suspense', 'Superhero'],
//         myReview: function () {
//             console.log('This is Batman! Michael Keaton is Batman! 4/4 stars!');
//         }
//     },
// ];
/** Once you added the additional movies, complete the following:
 *  Loop through the array to display and console log the 'Release Date: month / date / year'.
 *
 *  Loop through the array to display and console log the 'Title, ReleaseDate, Director, myReview'
 *
 * Loop through the array to display and console log the 'Genre'
 *
 *  Loop through the array to display and console log the 'Director: FirstName LastName'
 */
// BONUS
/** TODO:
 * HEB has an offer for the shoppers that buy products amounting to
 * more than $200. If a shopper spends more than $200, they get a 12%
 * discount. Write a JS program, using conditionals, that logs to the
 * browser, how much Stephen, Justin and Leslie need to pay. We know that
 * Justin bought $180, Stephen $250 and Leslie $320. Your program will have to
 * display a line with the name of the person, the amount before the
 * discount, the discount, if any, and the amount after the discount.
 *
 * Uncomment the lines below to create an array of objects where each object
 * represents one shopper. Use a foreach loop to iterate through the array,
 * and console.log the relevant messages for each person
 */
/*
var shoppers = [
        {name: 'Justin', amount: 180},
        {name: 'Stephen', amount: 250},
        {name: 'Leslie', amount: 320}
    ];

 */

//-------OBJECTS, LOOPS, FUNCTIONS EXERCISE-----------
//----------------------------------------------------

//-------OBJECT MOVIE W/ 4 PROPERTIES------------
// Create an object called movie. Include four properties
// *  Console log each property.

var movie = ["title", "protagonist", "antagonist", "genre"];

// console.log(movie[0]);
// console.log(movie[1]);
// console.log(movie[2]);
// console.log(movie[3]);






//-------OBJECT MOVIE W/ PROPERTIES OF TITLE, PROTAGONIST, ANTAGONIST, GENRE--------
// Create an object called movie include four properties of title, protagonist, antagonist, genre
// *  console log each property.

var movie =
    {
        title: "The Dark Night",
        protagonist: "Batman",
        antagonist: "Joker",
        genre: "Superhero",
    }

// console.log(movie.title)
// console.log(movie.protagonist)
// console.log(movie.antagonist)
// console.log(movie.genre)

//-----------PET COMPANION OBJECT--------------
// Create a pet/companion object include the following properties:
//     *  string name, number age, string species, number valueInDollars,
// *  array nickNames, vaccinations: date, type, description.

var petCompanion = [
    {
        name: "Bob",
        age: 2,
        species: "dog",
        valueInDollars: "$200",
        nickNames: ["Duke", "Bolt", "Baldo" ],
        vaccination: {
            date: "02/12/2021",
            type: "rabies",
            description: "All dogs should be vaccinated for rabies at approximately 14 weeks of age"
        }

    }
];


//Exercise
// Add 3 additional objects to the array below called movies. Be sure to include five properties: title, releaseDate, director, genre, and myReview.
// *  The myReview property should be a function

// const movies = [
//     {
//         title: 'Batman',
//         releaseDate: {
//             month: June,
//             date: 23,
//             year: 1989
//         },
//         director: {
//             firstName: 'Tim',
//             lastName: 'Burton'
//         },
//         genre: ['Action', 'Suspense', 'Superhero'],
//         myReview: function () {
//             console.log('This is Batman! Michael Keaton is Batman! 4/4 stars!');
//         }
//     },
//     {
//         title: 'Deadpool',
//         releaseDate: {
//             month: June,
//             date: 23,
//             year: 1989
//         },
//         director: {
//             firstName: 'Alice',
//             lastName: 'Burton'
//         },
//         genre: ['Action', 'Suspense', 'Superhero'],
//         myReview: function () {
//             console.log('This is Batman! Michael Keaton is Batman! 4/4 stars!');
//         }
//     },
//     {
//         title: 'Hulk',
//         releaseDate: {
//             month: June,
//             date: 23,
//             year: 1989
//         },
//         director: {
//             firstName: 'Bill',
//             lastName: 'Burton'
//         },
//         genre: ['Action', 'Suspense', 'Superhero'],
//         myReview: function () {
//             console.log('This is Batman! Michael Keaton is Batman! 4/4 stars!');
//         }
//     },
//     {
//         title: 'Ironman',
//         releaseDate: {
//             month: June,
//             date: 23,
//             year: 1989
//         },
//         director: {
//             firstName: 'Cathy',
//             lastName: 'Burton'
//         },
//         genre: ['Action', 'Suspense', 'Superhero'],
//         myReview: function () {
//             console.log('This is Batman! Michael Keaton is Batman! 4/4 stars!');
//         }
//     },
//
// ];
// /** Once you added the additional movies, complete the following:
//  *  Loop through the array to display and console log the 'Release Date: month / date / year'.

// console.log(movies[0].release.date)

//
for (var i = 0; i < movies.length; i++) {
//     console.log(  "Release Date: " + movies[i].releaseDate.month + "/" +
//     movies[i].releaseDate + "/" +
//     movies[i].releaseDate.year);
}

// forEach ANGELA'S SOLUTION
movies.forEach(function (movie) {
    // console.log(`Release Date:
    // ${movie.releaseDate.month} / ${movie.releaseDate.date} / ${movie.releaseDate.year}`
    // );
});




//  *
//  *  Loop through the array to display and console log the 'Title, ReleaseDate, Director, myReview'

for (var i = 0; i < movies.length; i++) {
    console.log(`Title: ${movies[i].title}\nRelease Date: ${movies[i].releaseDate.year}`)
}




//  *
//  * Loop through the array to display and console log the 'Genre'

//  *
//  *  Loop through the array to display and console log the 'Director: FirstName LastName'

//  */


const movies = [
    {
        title: 'Batman',// string
        releaseDate: { // object
            month: June,
            date: 23,
            year: 1989
        },
        director: {
            firstName: 'Tim',
            lastName: 'Burton'
        },
        genre: ['Action', 'Suspense', 'Superhero'], //array
        myReview: function () {
            console.log('This is Batman! Michael Keaton is Batman! 4/4 stars!'); // function
        }
    },
    {
        title: 'Deadpool',
        releaseDate: {
            month: June,
            date: 3,
            year: 1990
        },
        director: {
            firstName: 'Alice',
            lastName: 'Burton'
        },
        genre: ['Action', 'Suspense', 'Superhero'],
        myReview: function () {
            console.log('This is Deadpool! Michael Keaton is Batman! 4/4 stars!');
        }
    },
    {
        title: 'Hulk',
        releaseDate: {
            month: June,
            date: 4,
            year: 1991
        },
        director: {
            firstName: 'Bill',
            lastName: 'Burton'
        },
        genre: ['Action', 'Suspense', 'Superhero'],
        myReview: function () {
            console.log('This is Hulk! Michael Keaton is Batman! 4/4 stars!');
        }
    },
    {
        title: 'Ironman',
        releaseDate: {
            month: June,
            date: 5,
            year: 1992
        },
        director: {
            firstName: 'Cathy',
            lastName: 'Burton'
        },
        genre: ['Action', 'Suspense', 'Superhero'],
        myReview: function () {
            console.log('This is Ironman! Michael Keaton is Batman! 4/4 stars!');
        }
    },

];

// Bonus----
// array with two properties: name and amount


var shoppers = [
    {name: 'Justin', amount: 180},
    {name: 'Stephen', amount: 250},
    {name: 'Leslie', amount: 320}
];
// need to creat a function to determine discount

function calculateDiscount(amount) {
    var finalAmount = 0;
    var amountOff = 0;

    //set up conditional

    if (amount > 200) {
        amountOff = amount * .12;
        finalAmount = amount - amountOff;
    }
    else {
        finalAmount = amount;
    }
    // did return instead of console.log
    return {
        finalAmount: finalAmount,
        amountOff: amountOff,

    };
}


function displayShopperInfo(shoppers) {
    shoppers.forEach(function (shopper) {
        var amount = calculateDiscount(shopper.amount);
        var message = "";

        message += shopper.name + " bought $";
        message += shopper.amount.toFixed(2);
        message += " and got $" + amount.amountOff.toFixed(2);
        message += " off and will pay $";
        message += amount.finalAmount.toFixed(2) + ".";


        console.log(message);





    })
}

displayShopperInfo(shoppers);

























"use strict";

/*
map-
filter-
reduce-
 */

var numbers = [11, 20, 33, 40, 55, 60, 77, 80, 99, 100]

var evens = []
var odds = []


//JS version
//js for loop
for(var i = 0; i < numbers.length; i++) {
    //if all the even numbers
    if (numbers[i] % 2 === 0) {
        //add the index from numbers to evens
        evens.push(numbers[i]);
    }
    else {
        //add the index from numbers to odds
        odds.push(numbers[i]);
    }

}


// console.log(evens);
// console.log(odds);
// es6 filter version
var evensFiltered = numbers.filter(function (n) {
    return n % 2 === 0;
})
// console.log(evensFiltered);

var oddsFiltered = numbers.filter(function (n) {
    return n % 2 !== 0;
})
// console.log(oddsFiltered);

//map
//write code that produces a new array of numbers with
//each number multilied by 10

// var numbersTimesTen = numbers.map(function (num) {
//     return num * 10;
// })
// console.log(numbersTimesTen);

//use arrow
const numbersTimesTen =numbers.map(num => num * 10);
// console.log(numbersTimesTen);

// REDUCE
const myArray = [1, 2, 3, 4, 5];
// const sum = myArray.reduce(function (acc, currentNumber) {
//     return acc + currentNumber;
// }, 0);
// // console.log(sum);

//same example, converted to arrow function

const sum = myArray.reduce((acc, currentNumber) => acc + currentNumber, 0);


// console.log(sum);


//creat an array of objects
const officeSales = [
    {
        name: "Jim Halpert",
        sales: 500
    },
    {
        name: "Dwight Schrut",
        sales: 750
    },
    {
        name: "Ryan Howard",
        sales: 150
    },
]

//todo: using reduce,get the total amount of sales for the week...use arrow
const money = officeSales
    .reduce((acc,person) => acc + person.sales,0);
console.log(money)









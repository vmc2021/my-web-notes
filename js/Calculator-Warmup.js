


function volume_sphere() {
    var volume;
    var radius = document.getElementById('radius').value;

    console.log(radius);

    // taking the value of radius from the form,
    // and converting it to a number
    radius = Math.abs(radius);

    // formula for volume of a sphere
    // V = 4/3 * pi * r^3
    // volume = (4/3) * Math.PI * Math.pow(radius, 3);

    volume = (4/3) * Math.PI * (radius * radius * radius);
    console.log(volume);

    // used this to get 4 decimal places
    volume = volume.toFixed(4);

    var answer = document.getElementById("answer");
    answer.innerHTML = volume;

    return false;
}


window.onload = document.getElementById('MyForm').onsubmit = volume_sphere;


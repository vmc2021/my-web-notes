// /**
//  * TODO: Change the body's background color to skyblue
//  */
//answer:



//second solution
document.body.style.backgroundColor = "skyblue";



// /**




//  * TODO: Console log the section with an id of container
//  */
//answer:
var container = document.getElementById("container");

// console.log(container)


// /**
//  * TODO: Console log the section with an id of container using querySelector. What is a querySelector?
//  */
// /**
var container1 = document.querySelector("#container");
// console.log(container1);




//  * TODO: Give the div with the class of header the text of "Hello World"
//  */

var header1 = document.getElementsByClassName("header");
header1.item(0).innerHTML = "Hello World";


// /**
//  * TODO: Console log all of the list items with a class of "second"
//  */
// /**

var listItems = document.getElementsByClassName("second");
console.log(listItems);




//  * TODO: Change the font family of all of the list items with a class of "second"
//  */
// /**
// listItems.item(0).style.fontFamily = "Fantasy";
// listItems.item(1).style.fontFamily = "Fantasy";
//other  way
for (var i =0; i < listItems.length; i++) {
    listItems.item(i).style.fontFamily = "Cursive"
}


//  * TODO: Give the section with an id of container the text of "Go CodeBound!"
//  */
// /**

var container2 = document.getElementById("container");
container2.innerHTML += "Go CodeBound!";



//  * TODO: Create a new li element, give it the text "four",
//   append it to the ul element
//  */
// /**
// jose's solution
var newItem = '<li class ="fourth">four</li>'; //create a <li> node
var uList = document.getElementsByTagName('ul'); // targets the <ul> element
uList.item(0).innerHTML += newItem; //adds <li class="four">four</li> to the <ul>




//  * TODO: Give the div with an class of footer the text of "This is my Footer!"
//  */
// similar to Go CodeBound exercise
//access the dom with document, then,



var footer = document.getElementsByClassName("footer");

footer.item(0).innerHTML = "This is my Footer!";



//Bonus
// use setTimeOut for message
// name a variable name what makes sense to you


var message = setTimeout(function () {
    alert("Welcome to the Dom Exercise!");
},5000);
















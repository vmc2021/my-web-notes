//use console.log after a request

//Load users in our console.log
$.get("https://jsonplaceholder.typicode.com/users",{

}).done(function (data) {
    //what do we want to do with data? log it to check data is working
    console.log(data);//it works: Array10
    console.log(data.name); // undefined, we need the index
    //get "Nicholas Runolf" in the console log
    console.log(data[7].name); // index to the left of log
    //get "Gwenborough" from the first object of the array
    console.log(data[0].address.city);

    //create variable and assign it to our function "displayUsers"

    var renderData = displayUsers(data);
    //target our id "users" and append renderData variable using jquery
    $("#users").html(renderData);

});

//render information
function displayUsers(users) {
    //local variable and assing to an empty string
    var userOnHTML = "";
    //loop through the array
    users.forEach(function (user) {
        //call the local variable,
        // and dynamically create the html...
    userOnHTML += `
        <div class="user">
        <h3>Employee Name</h3>
        <a href="http://${user.website}">View website</a>
        <p>User Info:</p>
        <ul>
        <li>
        <p>Username: ${user.username}</p>
        </li>
        <li>
        <p>Address: ${user.address.street} ${user.address.city}</p>
        </li>
        </ul>
        </div>
    `

    })
//outside of forEach loop
    //return statement to return our variable "userOnHTML"
    return userOnHTML;


}




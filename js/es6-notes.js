"use strict";
// // Es6
// //---------------------Exponent Operator------------------------------
// //javascript
// Math.pow(2,5);
// console.log(Math.pow(2,5)); //32
//
// //es6
// console.log(2 ** 5); //32
//
// //var vs const
//
// let instructor = "Stephen";
// instructor = "Justin";
//
// console.log(instructor); // Justin
//
//
//
// //------------Template Strings vs concatenation-----------------------
// //A. concatenation
// const cohort = "Delta";
// // console.log("Hello, and welcome to " + cohort + "!");
//
// //template strings
// // console.log(`Hello,`)
//
//
// //-------------for... of---------------------------------------------
//syntax for... of
// for (let / element of iterable) {   }
//use for arrays and collection of objects

const arr = ["one", "two", "three"];
for (let e of arr) {
    // console.log(e);
}
//example above in a for loop? Javascript example
for(var i = 0; i < arr.lenght; i++) {
    // console.log(arr[i]);
// }
// // 2nd Example
// Todo: Using a for loop, log each day of the week
// const days = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"]

//
// let daysOnHTML = "";
//
// for (let e of days) {
//     // console.log(e);
//
// //get days onto the html web page
//
//
//     daysOnHTML += `
//     <ul>
//
//     <li>${e}</li>
//
//
//     </ul>
//     `;
//
//     $("#days").html(daysOnHTML);
//
//     }
//
// //---------------arrow functions-----------------------------------------
// //---------------shorthand function syntax
// //used in Angular
//
// // vanilla js function
// function sayHello(name) {
//     return `Hello ${name}`;
// }
// // console.log(sayHello("Delta"));
//
//
//
// //using ES6 converting to arrow function (=>) is return and curly brace
// let sayHelloAgain = name => `Hello ${name}`;
// // name is the parameter => returns Hello and whatever the value of name is
// // console.log(sayHelloAgain("Delta"));
//
// //js function with no parameters
//
//
// //es6 arrow function with no parameters
// let returnTwoAgain = () => 2;
// //console.log(returnTwoAgain());
//
//
// //--------------------------typeof---------------------------------------
// // var x =1;
// // console.log(typeof x); //number
// // var y = "Delta"; //string
// // console.log(typeof y);
// // var z = true; // boolean
// // console.log(typeof z);
// // var a;
// // console.log(typeof a);
//
//
//
//
// //-------------Default Function Parameters values---------------------------
function greeting(cohort) {
    if (typeof cohort === "undefined") {
        cohort = "World";
    }
    return console.log(`Hello ${cohort}`);
}

greeting();//Hello World

greeting("Delta");


//example above in es6
const greetingAgain = (cohort = "World") => console.log(`Hello ${cohort}`);
greetingAgain(); //Hello World
greetingAgain("Echo"); //Hello Echo

//second example of default parameters
var addArgA = (num1,num2) => {
    if (num1 === undefined) {
        num1 = 2;
    }
    if (num2 === undefined) {
        num2 = 2;
    }
    return num1 + num2;
};
// console.log(addArgA()); //4
// console.log(addArgA(1)); // 1+2=3
// console.log(addArgA(5,9)); //14
//
//
// //-----------------OBJECT PROPERTY VARIABLE ASSIGNMENT SHORTHAND-----------------
// //-----Objects from JS review
    var person = {
        //property names (name and age)
        name: "Delta",
        age: 2021
    };

    // objects from es6 shorthand
    const name = "Delta";
    const age = 2021;

    const person = {
        //pass property inside an object
        name,
        age
    };
    //

// //-----------OBJECT DESTRUCTURING--------------------------------
// //--shorthand for creating variables from object properties
// //javascript
// var person = {name: "Delta", age: 2};
// var personName = person.name; //Delta
// var personAge = person.age; //2
//
// //es6 version
// const person = {
//     name: "Delta",
//     age: 2
// };
// const {name, age} = person; //es6
//
// //---DESTRUCTURING ARRAYS------------------------------------------
// var myArray = [1, 2, 3, 4, 5];
// const [a, y, thirdIndex, bob, adam] = myArray;
// console.log(bob) // 4. assigned to 4th placement of the array


//===============ARROW FUNCTION DRILL=====================================

//TODO: CREATE an arrow func that'll take in a
//length and width
// and returns the area

//js version
function area(length, width) {
    return length * width;
}
console.log(area(10, 20)); //200
//convert to arrow function
const areaAgain = (length, width) => length * width;
console.log(areaAgain(10, 20)); //200

//TODO: ARROW FUNCTION THAT;LL TAKE IN A
//length and width, and returns the parameter.
const perimeter = (width, height) => (2*width) + (2*height);
console.log(perimeter(6, 2));

//convert JS function to Arrow
function helloCohort(greeting, cohort) {
    if (typeof greeting === "undefined") {
        greeting = "Good Day";
    }
    if (typeof cohort === "undefined") {
        cohort = "Delta";
    }
    return greeting + "" + cohort;
}

const helloCohortAgain = (greeting = "Good Day",
cohort = "Delta") => `${greeting} ${cohort}`; // return var using template strings
console.log(helloCohortAgain());


















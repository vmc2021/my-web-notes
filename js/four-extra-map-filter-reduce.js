'use strict';

const companies = [
    {name: 'Walmart', category: 'Retail', start_year: 1962, location: { city: 'Rogers', state: 'AR' }},
    {name: 'Microsoft', category: 'Technology', start_year: 1975, location: { city: 'Albuquerque', state: 'NM' }},
    {name: 'Target', category: 'Retail', start_year: 1902, location: { city: 'Minneapolis', state: 'MN' }},
    {name: 'Wells Fargo', category: 'Financial', start_year: 1852, location: { city: 'New York', state: 'NY' }},
    {name: 'Amazon', category: 'Retail', start_year: 1994, location: { city: 'Bellevue', state: 'WA' }},
    {name: 'Capital One', category: 'Financial', start_year: 1994, location: { city: 'Richmond', state: 'VA' }},
    {name: 'CitiBank', category: 'Financial', start_year: 1812, location: { city: 'New York', state: 'NY' }},
    {name: 'Apple', category: 'Technology', start_year: 1976, location: { city: 'Cupertino', state: 'CA' }},
    {name: 'Best Buy', category: 'Retail', start_year: 1966, location: { city: 'Richfield', state: 'MN' }},
    {name: 'Facebook', category: 'Technology', start_year: 2004, location: { city: 'Cambridge', state: 'MA' }},
];



// map all of the company names
const companyNames = companies.map(function (company) {
   return company.name;
})
console.log(companyNames);
//arrow
// const companyNames = companies.map(company => company.name);



// filter all the company names started before 1990
const before = companies.filter(function (company) {
    if (company.start_year < "1990") {
        return company;
    }
});
// console.log(before);
//arrow with Map
const beforeTime = companies.filter(c => c.name && c.start_year < 1990);
//using map
// const beforeTime = beforeTime.map(company => company.name);
// console.log(beforeTime);




// filter all the retail companies
const allRetail = companies.filter(function (company) {
    return company.category === "Retail";
})
// console.log(allRetail);
//arrow
// const allRetail = companies.filter(r => r.category === "Retail");
// console.log(allRetail);

// find all the technology and financial companies
const techAndfin = companies.filter(company => company.category === "Technology" || company.category === "Financial");
// console.log(techAndfin)


// filter all the companies that are located in the state of NY. location is an object within an object
const filterNY = companies.filter(function (company) {
    return company.location.state === "NY";
})
// console.log(filterNY);
//arrow
// const filterNY = companies.filter(company => company.location.state === "NY");


// find all the companies that started on an even year.
var evenFiltered = companies.filter(function (c) {
    return c.start_year % 2 === 0;
});
//arrow
var evenFiltered = companies.filter(c => c.start_year % 2 === 0);
// console.log(evenFiltered);


// use map and multiply each start year by 2

const multiply = companies.map(company => company.start_year * 2);


// find the total of all the start year combined.
const yearSum = companies.reduce((acc,company) => acc + company.start_year,0);
// console.log(yearSum);


// display all the company names in alphabetical order
var alphaName = companies.map(company => company.name);
// console.log(alphaName.sort());

// display all the company names for youngest to oldest.
const youngOld = companies.map(company => company.start_year);
console.log(youngOld);

const reverseList = youngOld.reverse();
console.log(reverseList);

const youngToOld =  companies.map(function (company) {
    return company.start_year + "" + company.name;
});
console.log(youngToOld.sort().reverse());





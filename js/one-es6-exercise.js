// ES6 EXERCISE
// Complete the following in a new js file name es6exercise.js
/*
 * Complete the TODO items below
 */



const developers = [
    {
        name: 'stephen',
        email: 'stephen@appddictionstudio.com',
        languages: ['html', 'css', 'javascript', 'php', 'java']
    },
    {
        name: 'karen',
        email: 'karen@appddictionstudio.com',
        languages: ['java', 'javascript', 'angular', 'spring boot']
    },
    {
        name: 'juan',
        email: 'juan@appddictionstudio.com',
        languages: ['java', 'aws', 'php']
    },
    {
        name: 'leslie',
        email: 'leslie@codebound.com',
        languages: ['javascript', 'java', 'sql']
    },
    {
        name: 'dwight',
        email: 'dwight@codebound.com',
        languages: ['html', 'angular', 'javascript', 'sql']
    }
];



// TODO: replace the `var` keyword with `const`, then try to reassign a variable as a const
// TODO: fill in your name and email and add some programming languages you know to the languages array

//given:
// var name
// var email
// var languages

//--------my answer
// const name
// const email
// const languages

const name = "victor"
const email = "victor@codebound.com"
const languages = ""


//=======correct answer=======



// TODO: rewrite the object literal using object property shorthand
developers.push({
    name: name,
    email: email,
    languages: languages
});

console.log(developers);

//---------------my answer

// let developers.push = {name,email, languages}



//=======the correct answer=========
developers.push({
    name,
    email,
    languages,
});
console.log(developers);




// TODO: replace `var` with `let` in the following variable declarations
var emails = [];
var names = [];

//----my answer----------
// const emails = [];
// const names = [];



// TODO: rewrite the following using arrow functions
developers.forEach(function(developer) {
    return emails.push(developer.email);
});
console.log(emails);
//---my answer--------

  // let   developers.forEach(developer) => return email.s


//=======correct answer=======
            developers.forEach(developer => emails.push(developers.email));
            console.log(emails);

developerTeam.forEach(function(developer) {
    return names.push(developer.name);
});
console.log(names);
//---my answer------------------

developer.froEach(developer => names.push(developer.name));
console.log(names);



//========correct answer=================




developers.forEach(function (developer) {
    return languages.push(developer.languages);
});
//------my answer-----------------------------






//=========correct answer=============




// TODO: replace `var` with `const` in the following declaration
var developerTeam = [];
//-----my answer----------




//======correct answer=============

// const developerTeam = [];




developers.forEach(function(developer) {
    // TODO: rewrite the code below to use object destructuring assignment
    //       note that you can also use destructuring assignment in the function
    //       parameter definition



// given:
// const name = developer.name;
//     const email = developer.email;
//     const languages = developer.languages;

    //====correct answer======
const {name, email, languages} = developer;




    // TODO: rewrite the assignment below to use template strings
    developerTeam.push(name + '\'s email is ' + email + name + ' knows ' + languages.join(', '));

//========answer=======================================================================
     developer.Team.push(`${name}'s email is ${email} ${name} knows ${languages.join(', ')}`);


});
console.log(developerTeam);




// TODO: Use `const` for the following variable
var list = '<ul>';

//----answer-------
// const list = '<ul>';




// TODO: rewrite the following loop to use a for..of loop
developers.forEach(function (developer) {
    // TODO: rewrite the assignment below to use template strings


    list += '<li>' + developer + '</li>';
});

for (let developer of developerTeam) {
    list += `<li></li>`;
}


list += '</ul>';
document.write(list);
Collapse




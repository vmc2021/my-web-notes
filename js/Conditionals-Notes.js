//CONDITIONALS

//If Statement Example 1
//Syntax
//  var numberOfLives = 0;
// if (numberOfLives === 0){ //condition is not met
//
//     alert("Game over!");
// }
// Example 2------------------p
// // variable, condition, else
//
// var score = 8;
//
//     if(score < 10){
//         console.log("Nice job, you scored more than 10")
//     }
//     else {
//         console.log("Try again... loser")
//     }
//     //Example 3--------------------
// // variable, if condition, else if condition, else
//
// var age = 11;
//     if ( age > 18 && age < 21) {
//         console.log("Welcome to the club, don't drink");
//     }
//     else if (age >= 21) {
//         console.log("Welcome to the club, don't get drunk");
//     }
//     else {
//         console.log("Beat it kid");
//     }
//Example 4---------------

// var input = prompt("Whats your favorite fast food");
//
// if (input === "Pizza" || input === "pizza") {
//     console.log("Cowabunga!")
// }
//
// else if (input === "Hamburger") {
//     console.log("Welcome to Good Burger!");
// }
//
// else if (input === "Wings"){
//     console.log("Add some hot sauce onto that!");
// }
//
// else {
//     console.log("Eww...");
// }

// ----------------Switch Statement Notes--------------
// another method for writing conditional statement

//example
var color = "Red";

switch (color) {
//set up cases
    case "Red": // if color is red is met, it will stop after alert(). if not it continues
        alert("You chose Red");
        break;
    case "Green":
        alert("You chose Green");
        break;
    default:
        alert("You didn't select a cool color");
        break;
        // you get "You chose Red"



}















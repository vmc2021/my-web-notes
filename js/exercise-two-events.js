/* jQuery Events
// Complete the following in a new js file named jquery-events-exercise.js
// NOTE: You'll also name a new html file, name this jquery-events-exercise.html
*/

// Add jQuery code that will change the background color of a 'h1' element when clicked.
$('h1').click(function(){
    $('h1').css('background-color','red');
})

// Make all paragraphs have a font size of 18px when they are double clicked.

$("p").dblclick(function () {
    $("p").css("font-size", "18px")
})


// Set all 'li' text color to green when the mouse is hovering,
// reset to black when it is not.

$("li").hover(function(){
    $(this).css("color", "green");
}, function(){
    $(this).css("color", "black");
});

// Add an alert that reads "Keydown event triggered!" everytime a user pushes a key down

// $("#my-form").keydown(function () {
//     alert("Keydown event triggered!");
// });


// Stephen's Solution
// $(document).keydown(function () {
//     alert("keydown event triggered!");
// });


// Console log "Keyup event triggered!" whenever a user releases a key.

// $("#my-form").keyup(function () {
//     console.log("Keyup event triggered!")
// });
//stephens solution
// $("body").keyup(function () {
//     console.log("Keyup event triggered!")
// });



// BONUS: Create a form with one input with a type set to text.
// Using jQuery, create a counter for everytime a key is pressed.


// //stephens solution
// var i = 0;
// $(document).keydown(function () {
//     $("#counter").text(i += 1);
// });


/*more practice here/////////////
 */
















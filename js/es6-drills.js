/* ES6 DRILLS */
// Complete the following in a new js file named 'es6-drills.js'
const petName = 'Chula';
const age = 14;


// REWRITE THE FOLLOWING USING TEMPLATE STRINGS ${something}
// console.log("My dog is named " + petName +
//     ' and she is ' + age + ' years old.');
console.log(`"My dog is named ${petName} "and she is ${age} "years old`)



// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function addTen(num1) {
//     return num1 + 10;
// }
const addTen = num1 => num1 + 10;
console.log(addTen(20))
// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function minusFive(num1) {
//     return num1 - 5;
// }
const minusFive = num1 => num1 - 5;
console.log(minusFive(9))
// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function multiplyByTwo(num1) {
//     return num1 * 2;
// }
const multiplyByTwo = num1 => num1 * 2;
console.log(multiplyByTwo(2))



// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// const greetings = function (name) {
//     return "Hello, " + name + ' how are you?';
// };

const greetings = name => `"Hello," ${name} 'how are you?'`;
console.log(greetings("Delta"));


// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function haveWeMet(name) {
//     if (name === 'Bob') {
//         return name + "Nice to see you again";
//     } else {
//         return "Nice to meet you"
//     }
// }
// answer 1--------------
const haveWeMet = name => {
    if (name === "Bob") {
        return `${name} Nice to see you again`;
    }
    else {
        return "Nice to meet you";
    }
}
// 2nd solution
//Ternary Operator
// syntax: condition ? message if condition is true : message if condition is not true
const haveWeMet1 = name => (name === "Bob") ? "Nice to see you again" : "Nice to meet you";
console.log(haveWeMet1("Bob"));


var scores = [80, 90, 75];
var total = 0;
// USING A FOR...OF, CONSOLE LOG THE SUM OF THE ARRAY scores

for (let score of scores) {
    total += score;
}
console.log(total);

//------BONUS----BONUS-------
//WRAP EVERYTHING IN backTICKS
// BONUS:
    // REWRITE THE FOLLOWING IN TEMPLATE STRINGS
var li = '<li>' +
    '<div class="row">' +
    '<div class="col-md-4">' +
    '<img src="' + moviePoster + '" height="250" alt="" />' +
    '</div>' +
    '<div class="col-md-8">' +
    '<h2>' + movieTitle + '</h2>' +
    '</div>' +
    '</div>' +
    '</li>';

var liWithTemplateStrings =`
    <li>
        <div class="row">

                <div class="col-md-4">
                <img src="${moviePoster}" height="250" alt="" />
                </div>
                <div class="col-md-8">
                <h2>${movieTitle}</h2>
                </div>
        </div>

    </li>
`;











"use strict";



// CREATE A FILE NAMED 'promises-exercise.js' INSIDE OF YOUR JS DIRECTORY
//========================================================
// Exercise 1:
// Write a function name 'wait' that accepts a number as a parameter, and returns
// a PROMISE that RESOLVES AFTER THE PASSED NUMBER OF MILLISECONDS
    // EX. wait(4000).then(() => console.log('You will see this after 4 seconds.'));
    // EX. wait(8000).then(() => console.log('You will see this after 8 seconds.'));

//delay is the parameter
// const wait = (delay) => new Promise((resolve, reject) => {
//     //executer function
//     setTimeout(() => {
//         resolve(`You will see this after ${delay / 1000} seconds`)
//     }, delay);
// });
//
// wait(10000).then((data) => console.log(data));

//OTHER WAY OF DOING IT
function waitMessage(delay) {
    return new promise()
}







//========================================================
// Exercise 2:
// Write a function testNum that takes a number as an argument/parameter and returns a Promise that tests if the value is less than or greater than the value 10.

// const testNum = new Promise((resolve, reject) => {
//     let num = 5
//     if ( num < 10) {
//       resolve("Less than")
//     }else {
//         reject("Greater than")
//     }
// });
// console.log(testNum);

//JOSE'S CHAINING






//==================================================================
// Exercise 3:
    //Write two functions that use Promises that you can chain! The first function, makeAllCaps(), will take in an array of words and capitalize them, and then the second function, sortWords(), will sort the words in alphabetical order. If the array contains anything but strings, it should throw an error.

//STEPHEN'S SOLUTION
// const myArray = ["apples", "oranges", "grapes", "bananas"];
//
// const makeAllCaps = array => new Promise((resolve, reject) => {
//     let arrayCaps = array.map(word => {
//         if (typeof word === "string") {
//             return word.toUpperCase();
//         }
//         else {
//             reject("Error: Item is not a string");
//         }
//     });
//     resolve(arrayCaps);
// });
//
// const sortWords = array => new Promise((resolve, reject) => {
//     if (array) {
//         array.forEach((e) => {
//             if (typeof e !== 'string') {
//                 reject("Error!")
//             }
//         });
//         resolve(array.sort());
//     }
//     else {
//         reject("Something went wrong with sorting the words");
//     }
// });
//
// makeAllCaps(myArray)
//     .then(sortWords)
//     .then((data) => console.log(data))
//     .catch((error) => console.log(error));





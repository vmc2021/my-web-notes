"use strict";

//PROMISES

/*
Three states of a promise:
-pending
-resolved
-rejected
 */

//example
//
var isMomHappy = true;

//promise
var willGetANewPhone = new Promise(function (resolve, reject) {
    if (isMomHappy === true) {
        var phone = {   // phone is the object
            brand: "Samsung",
            color: "Black"
        };
        resolve(phone);
    }
    else {
        var reason = new Error("Your grades are not good!");
        reject(reason);
    }
});
// // console.log(willGetANewPhone);
//
var promise = new Promise(function (resolve, reject) {
    setTimeout(function () {
        resolve("Promise is resolved")
        // reject("Promise has been rejected")
    }, 5000);
});
// console.log(promise);

//==========================================================================

//creat a promise using a variable with a boolean value "true|false"


let goodKid = true;

const getsCake = new Promise(function (resolve, reject) {
    if(goodKid === true) {
resolve("Here is a piece of chocolate cake");
    }
    else {
        reject("No cake. You're misbehaving");
    }
})
    // .then(data => console.log(data))
    // .catch(error => console.log(error));

// console.log(getsCake);
//============================================================================================
//make a request to external star wars API

    $.ajax("https://swapi.dev/api/people/1")
        //setting up a .then() if it resolves
        .then(function (data) {
            // console.log(data);
        })
        //setting upa .catch() if it rejects
        .catch(function (error) {
            // console.log(error + ": bad request to api");
        });

//====================================================================================================
const starbucksOrder = type => {
// parameter is ${type}, two promises let makeOrder and let processOrder

let makeOrder = new Promise((resolve, reject) => {
    setTimeout(function () {
        resolve(`coffee type: ${type} is ready!`);
    }, 2000);
});



let processOrder = new Promise((resolve, reject) => {
    setTimeout(function () {
        resolve(`Coffee type ${type} has been ordered and paid for`);
    }, 4000);
});

let enjoy = new Promise((resolve, reject) => {
    setTimeout(function () {
        resolve(`Enjoy your ${type}`);
    }, 6000);
})

return Promise.all([processOrder,makeOrder, enjoy]);



}

// starbucksOrder("espresso").then((data) => {
//     console.log(data[0]);
//     console.log(data[1]);
//     console.log(data[2]);
// }).catch((error) => console.log(error));
















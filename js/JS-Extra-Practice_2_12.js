EXTRA_PRACTICE
// =========================================================================
// Using a for loop, recite the song "99 bottles of beer" by decreasing
// EX. "99 bottles of beer on the wall, 99 bottles of beer. Take one down and pass it around, 98 bottles of beer on the wall."



for (var i = 100; i >= 0; i--) {

    console.log(i + " bottles of beer on the wall" i + " bottles of " +
    "beer. Take one down and pass it around,");
}




// =========================================================================
// Create a function that returns the longest string
// in the given array of string elements
// var arrayOfStrings = [
//     "hello",
//     "everybody",
//     "javascript",
//     "is",
//     "rad",
//     "ZOMG arrays of strings"
// ];


Math.max(...(x.map(el => el.length)));







// =========================================================================
// /**
//  * TODO:
//  * Create the following three functions, each will accept an array and
//  * return an an element from it
//  * - first: returns the first item in the array
//  * - second: returns the second item in the array
//  * - last: returns the last item in the array
//  *
//  * Example:
//  *  > first([1, 2, 3, 4, 5]) // returns 1
//  *  > second([1, 2, 3, 4, 5]) // returns 2
//  *  > last([1, 2, 3, 4, 5]) // return 5
//  */


console.log(arrayOfStrings[0]);
console.log(arrayOfStrings[1]);
console.log(arrayOfStrings[4]);




// =========================================================================
// var planetsString = "Mercury|Venus|Earth|Mars|Jupiter|Saturn|Uranus|Neptune";
// var planetsArray = planetsString.split("|");
/**
 * TODO PART 1:
 * Convert planetsString to an array, and save it in a variable named
 * planetsArray.
 * console.log planetsArray to check your work
 */

//"Mercury|Venus|Earth|Mars|Jupiter|Saturn|Uranus|Neptune"
var planetArray = ["Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"];

function planetArray() {
    console.log(planetArray);
}








// /**
//  * TODO PART 2:
//  * Create a string with <br> tags between each planet. console.log() your
//  * results. Why might this be useful?
//  *
//  *
//-------Part2--------------------

var planetString = "Mercury" + "<br /> " + "Venus" + "<br /> " + "Earth" + "<br /> " + "Mars" +
    "<br /> " + "Jupiter" + "<br /> " + "Saturn" + "<br /> " + "Uranus" + "<br /> " + "Neptune"






//  *
//  *
//  * BONUS:
//  * Create another string that would display your planets in an undordered
//  * list. You will need an opening AND closing <ul> tags around the entire
//  * string, and <li> tags around each planet.
//  */
// =========================================================================
/** TODO PART 1:
 * Create an array of objects that represent books and store it in a
 * variable named `books`. Each object should have a title and an author
 * property. The author property should be an object with properties
 * `firstName` and `lastName`. Be creative and add at least 5 books to the
 * array
 *
 * Example:
 * > console.log(books[0].title) // "Harry Potter"
 * > console.log(books[0].author.firstName) // "JK"
 * > console.log(books[0].author.lastName) // "Rowling"
 */
/**
 * TODO PART 2:
 * Loop through the books array and output the following information about
 * each book:
 * - the book number (use the index of the book in the array)
 * - the book title
 * - author's full name (first name + last name)
 *
 * Example Console Output:
 *
 *      Book # 1
 *      Title: Harry Potter
 *      Author: JK Rowling
 *      ---
 *      Book # 2
 *      Title: IT
 *      Author: Stephen King
 *      ---
 *      Book # 3
 *      Title: To Kill A Mockingbird
 *      Author: Harper Lee
 *      ---
 *      ...
 */
/**
 * Bonus:
 * - Create a function named `createBook` that accepts a title and author
 *   name and returns a book object with the properties described
 *   previously. Refactor your code that creates the books array to instead
 *   use your function.
 * - Create a function named `showBookInfo` that accepts a book object and
 *   outputs the information described above. Refactor your loop to use your
 *   `showBookInfo` function.
 */
// =========================================================================


















































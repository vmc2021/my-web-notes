/* JavaScript Loops
// Complete the following in a new js file named loops-exercise.js
*/
"use strict";
/** TODO: While Loops
 *   Create a while loop that uses console.log() to create the output shown below
 *   2
 *   4
 *   8
 *   16
 *   32
 *   64
 *   128
 *   256
 *   512
 *   1024
 *   2048
 *   4096
 *   8192
 *   16384
 *   32768
 *   65536
 */
/** TODO: For Loops
 *   Create a function name showMultiplicationTable that accepts a number and console.logs the multiplication table for that number
 *   Example:
 *   showMultiplicationTable(8) should output
 *   8 x 1 = 8
 *   8 x 2 = 16
 *   8 x 3 = 24
 *   8 x 4 = 32
 *   8 x 5 = 40
 *   8 x 6 = 49
 *   8 x 7 = 56
 *   8 x 8 = 64
 *   8 x 9 = 72
 *   8 x 10 = 80
 */
/** TODO: Create a for loop that uses console.log to create the output shown below.
 100
 95
 90
 85
 80
 75
 70
 65
 60
 55
 50
 45
 40
 35
 30
 25
 20
 15
 10
 5
 */
/** TODO: Prompt the user for an odd number between 1 and 50.
 * Prompt the user for an odd number between 1 and 50.
 *
 *
 * Use a loop and a break statement to continue prompting
 *   the user if they enter invalid input.
 *
 *
 *
 *   Use a loop and the continue statement to output
 *   all the odd numbers between 1 and 50,
 *   except for the number the user entered.
 *   Output should look like this:
 Number to skip is: 27
 Here is an odd number: 1
 Here is an odd number: 3
 Here is an odd number: 5
 Here is an odd number: 7
 Here is an odd number: 9
 Here is an odd number: 11
 Here is an odd number: 13
 Here is an odd number: 15
 Here is an odd number: 17
 Here is an odd number: 19
 Here is an odd number: 21
 Here is an odd number: 23
 Here is an odd number: 25
 Yikes! Skipping number: 27
 Here is an odd number: 29
 Here is an odd number: 31
 Here is an odd number: 33
 Here is an odd number: 35
 Here is an odd number: 37
 Here is an odd number: 39
 Here is an odd number: 41
 Here is an odd number: 43
 Here is an odd number: 45
 Here is an odd number: 47
 Here is an odd number: 49
 */
/** TODO:
 *  Write a for loop that will iterate from 0 to 20. For each iteration, it will check if the current number is even or odd,
 *  and report that to the screen (e.g. "2 is even").
 */
/**
 * TODO:
 *  Write a for loop that will iterate from 0 to 10. For each iteration of the for loop,
 *  it will multiply the number by 9 and log the result (e.g. "2 * 9 = 18").
 */
/** TODO: Break and ContinuePrompt the user for an odd number between 1 and 50. *
 *   Use a loop and a break statement to continue prompting the user if they enter invalid input.
 *   Use a loop and the continue statement to output all the odd numbers between 1 and 50, except for the number the user entered.
 */
/** BONUS:
 * Write a program that finds the summation of every number from 1 to num.
 * The number will always be a positive integer greater than 0.
 *
 * Ex.
 * summation(2) -> 3
 * 1 + 2
 *
 * summation(8) -> 36
 * 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8
 */
/** BONUS:
 * Create a for loop that uses console.log to create the output shown below.
 *
 * 1
 * 22
 * 333
 * 4444
 * 55555
 * 666666
 * 7777777
 * 88888888
 * 999999999
 */

//Exercise 1 ---- While Loop----------

    // var number = 2;
    // while (number <= 65536) {
    //     console.log(number)
    //
    //     number *= 2;
    // }

//Exercise 2 ----1st For Loops----------

// for ( var showMultiplicationTable = 80;
//       showMultiplicationTable >= 8;      // my answer
//       showMultiplicationTable += 8 ) {
//     console.log(`# ${showMultiplicationTable}` );
// }
// function showMultiplicationTable(input) {
//     //include for loop here
//     for (var i = 1; i <= 10; i++) { // we are incrementing by 1 on 3rd step)
//         var answer = input * i;
//         //temple string format
//         console.log(`${input} x ${i} = ${answer}`);
//
//     }
//
// }
// // call the function
// showMultiplicationTable(8)



//Exercise 3 -------2nd For Loop----------
// for (var number = 100; number >= 5; number -= 5 ) {
//     console.log(`# ${number}`);
// }

//stephens answer---------------

// for (var i = 100; i >= 5; i-= 5) {
//     console.log(i);
// }





//Exercise 4 ------Prompt a User----------
//Stephen used a Do While loop

// do {
//     var userInput = prompt("Enter an odd number between 1 - 50");
//     //check for an od number between 1 - 50
//
//     //condition below. is it met?
//     if (userInput % 2 != 0 && userInput >= 1 && userInput <= 50) {
//         break; // break out of loop if conditional is met
//
//     }
// }   while (true);
//     console.log("Number to skip is: " + userInput);
//
// // outside the do while loop
// // for loop increments i by 2 all the way to 50
//
// for (var i = 1; i <= 50; i+= 2) {
//     //if statement that checks if i matches the userInput value
//     if (i === Number(userInput)) {
//         // console.log("Yikes! Skipping Number: " + userInput);
//         console.log(`Yikes! Skipping numbers: ${userInput}`);
//         continue;
//     }
//
//     console.log("Here is an odd number: " + i);
//
//
// }



//Exercise 5 ------1st For Loop that iterates 0 to 20-------

//
// for (var i = 0; i <= 20; i++) {
//     if (i % 2 === 0) {
//         console.log(i + " is even."); //concatnation
//     }
//     else {
//         console.log(i + " is odd.");
//     }
//
// }

//Exercise 6 -------2nd Loop that iterates 0 to 10---------
// use multiplication table structure

for (var i = 0; i <= 10; i++) {
    console.log(i + "* 9 = " + (i * 9)); // concatenation inside console
}



//Exercise 7-------- Break and ContinuePrompt-------------

// skip duplicant


//------Bonus #1-----------------------------------------



//------Bonus #2-----------------------------------------

// use for loop------- google .toString method and .repeat method
// .toString press numbers together
// for (var i = 1; i <= 9; i++) {
//     i = i.toString();
//     console.log(i.repeat(i));
// }

//solution 2
var output = ""; // "_" is like a blank
for (var i = 1; i <= 9; i++) {
    output = "",
    for (var j = 1; j <= i; i++)



}











// var car = {
//
//   make: "Toyota",
//     model: "4Runner",
//     color: "Hunter Green",
// numberOfWheels: 5
//
// }
// ;console.log(car)
//
// // value for color
//
// console.log(car.color) //Hunter Green

//Nested Values

// var cars = [ // three elements inside(car array)
//     {
//         make: "Toyota",    // 5 features
//         model: "4Runner",
//         color: "Hunter Green",
//         numberOfWheels: 5,
//         features: ["Heated Seats", "Bluetooth", "Automatic Doors"], // [0], [1]
//         //use an array for list of features
//         alarm: function () {  //alarm is object method
//             alert("Sound the alarm")
//         }
//         //alarm is object method
//     },
//     {
//         make: "Honda",    // Honda is a value
//         model: "Civic",
//         color: "Black",
//         numberOfWheels: 4,
//         features: ["Great Gas Mileage", "FM/AM", "Still Runs"],
//         //use an array for list of features
//         alarm: function () {
//             alert("No alarm... sorry")
//         },
//         owner: { //object within object
//             name: "John Doe",
//             age: 35
//         }
//
//     },
//     {
//         make: "Ford",    // 5 features
//         model: "Bronco",
//         color: "Navy Blue",
//         numberOfWheels: 4,
//         features: ["Power Windows", "Bluetooth Radio", "GPS Navigation"],
//         //use an array for list of features
//         alarm: function () {
//             alert("Move, get out of the way")
//         },
//         owner: "Jane Doe",
//         age: 30,
//         address: {
//             street: "150 Alamo Plaza",
//             city: "San Antonio",
//             state: "Texas",
//         }
//
//     }
//
//
//
// ];

// log the honda object

// console.log(cars[1]); // var cars contains 3 elements
// its like console.log(names[1]) var names = ["stephen", "angela", ...]
// honda has the index number 1 or [1]

// log the model of the honda object only
// console.log(cars.[1].model);

//log the owner of the ford object....
// console.log(cars[2].owner);
// owner is object within an object


//log the city of the ford's owner...
// console.log(cars[2].owner.address.city);
//
// //log the second feature of the toyota object only...
// console.log(cars[0].features[1]); // bluetooth // car is an array, bluetooth is inside the features array
//
// // alarm function of the honda's object...
// console.log(cars[1].alarm()); // cars array, then honda[1]
// start at cars array, then honda's index number, then (dot), then alarm, which is a property inside honda object





// display features of all cars
//forEach

// cars.forEach(function (car) {
//
//     car.features.forEach(function (feature) {
//         console.log(feature);
//     })
//     // cars becomes car in function a
// });

// 'this keyword

// var videoGames = {};
//
// videoGames.name = "Super Mario";
//
// videoGames.company = "Super Mario";
//
// videoGames.genre = "Super Mario";
//
//
// // creat a method on the videoGame object
// videoGames.description =
//     function () {
//         alert("Video Game Name: " + this.name  // use concatenation or template strings
//         + "\nCompany of Video Name: " + this.company
//         + "\nGenre of Video Game: " + this.genre);
// //    (\n) new line to format a new line inside alert box
//     };
//
// // call the description property
// videoGames.description();

//// Create a function that will return how many whitespace characters
///are at the beginning and end of a string.
//What is trim?
//
// function whitespace(input) {
//     var length = input.length;
//     var trim = Number(input.trim().length);
//
//
//     console.log(Number(length - trim));
//
// }
//
// (whitespace(" stephen guedea "));












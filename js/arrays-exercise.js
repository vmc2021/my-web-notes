/* JAVASCRIPT ARRAYS */
// Complete the following in a new js file named arrays-exercise.js
"use strict";
/** TODO 1:
 *  Create a new EMPTY array named sports.**/
/** Push into this array 5 sports**/
/** Sort the array.**/
/** Reverse the sort that you did on the array.**/
/** Push 3 more sports into the array **/
/** Locate your favorite sport in the array and Log the index. **/
/** Remove the 4th element in the array **/
/** Remove the first sport in the array **/
/** Turn your array into a string  **/
/**
 * ----------------------------------------------------------------------
 * TODO 2: Create an array to hold your top singers/bands/etc.
 // *  const bands = ['AC/DC', 'Pink Floyd', 'Van Halen', 'Metallica'];
 1. console log 'Van Halen'
 2. add 'Fleetwood Mac' to the beginning of the array
 3. add 'Journey' to the end of the array
 */
// ----------------------------------------------------------------
/** Create a new array named cohort_name that holds the names of the people in your class**/
/** Log out each individual name from the array by accessing the index. **/
/** Using a for loop, Log out every item in the COHORT_NAME array **/
/** Refactor your code using a forEach Function **/
/** Using a for loop. Remove all instance of 'CodeBound' in the array **/
var arrays1 = ["fizz", "CodeBound", "buzz", "fizz", "CodeBound", "buzz"];
/**Using a forEach loop. Convert them to numbers then sum up all the numbers in the array **/
var numbers = [3, '12', 55, 9, '0', 99];
/**Using a forEach loop Log each element in the array while changing the value to 5 times the original value **/
var numbers = [7, 10, 22, 87, 30];
/** Create a function named removeFirstLast where you remove the first and last character of a string.**/
// Bonus---------------------------------------------------------------
/** Write a function named phoneNum that accepts an array of 10 integers (between 0 and 9). This will return a string of those numbers in a
 *  phone number format.
 *  phoneNum([7,0,8,9,3,2,0,1,2,3])=> returns "(708) 932-0123"
 */
// --------------------------------------------------
/** Create a function that will take in an integer as an argument and Returns "Even" for even numbers and returns "Odd" for odd numbers **/
// -----------------------------------------------
/** Create a function named reverseString that will take in a string and reverse it. There are many ways to do this using JavaScript built-in
 * methods. For this exercise use a loop.
 */
// ----------------------------------------------------
/**Create a function named converNum that will convert a group of numbers to a reversed array of numbers**
 * 386543 => [3,4,5,6,8,3]
 */
//Exercise Solutions-----------------


//  To Do #1--------------------------------------------------------------------------------
    // start new array: var sports[];

var sports = ["golf", "bowling", "tennis", "football", "soccer"];
    // console.log(sports);

//sort
// console.log(sports.sort()); // bowling, football, golf, soccer, tennis

//Reverse
sports.reverse();
// console.log(sports); // tennis, soccer, golf, football, bowling

//add to the array
sports.push("basketball", "cycling", "baseball"); // adds to the end of array
// console.log(sports);

// favorite sport, log the index
var indexElement = sports.indexOf("football");
// console.log(indexElement); // tennis=0, soccer=1, golf=2, football=3

// remove 4th element in array
// console.log(sports);
var remove4 = sports.splice(3,1);
// console.log(remove4);

//remove 1st sport of array
sports.shift();
// console.log(sports);


//turn your array in to a string

// console.log(sports.join(", "));

// To Do #2---------------------------------------------------------------------------------

const band = ["deftones", "green day", " guns n roses", "tool"];
    // console.log(band);
// console log van halen console.log(band[2]);
//
band.unshift("fleetwood mac");
// console.log(band);
//
// band.push("journey"); // adds to the end of array
// console.log(band);







// Create array called cohort_name--------------------------

 // var cohort_name ["victor", "alyssa", "jose", "angela"];


//log out
// console.log(cohort_name[0]);
// console.log(cohort_name[1]);
// console.log(cohort_name[2]);
// console.log(cohort_name[3]);

//use loop--------------

// for(var i = 0; i < cohort_name.lenght; i++) {
    // console.log(cohort_name[i]);
}

//refactor your code using forEach----------------
//remember syntax
// start with name of the array
//
// cohort_name.forEach( function (student_name) {
//     // console.log(student_name);
// }

// var arrays1 example---------------------------
// var arrays1 = ["fizz", "CodeBound", "buzz", "fizz", "CodeBound", "buzz"];


// for (var i = 0; i < arrays1.lenght; i++) {
//     if (arrays1[i] !== "CodeBound") {
//         // console.log(array1[i]);
//     }
// }
//using forEach loop----- Ask Stephen about this again
//
// var numbers = [3, '12', 55, 9, '0', 99];
// answer:--------------
// var sum = 0
//
// numbers.forEach( function (number) {
//     sum += parseInt(number); // use parseInt
// });
// console.log(sum); //178

// var numbers = [7, 10, 22, 87, 30];
//
// numbers.forEach(function (n) {
    // console.log(n * 5)
// });

//last example

function removeFirstLast(input) {

    var removeArray = input.split("");

    removeArray.shift();

    removeArray.pop();

    return removeArray.join("")
}
console.log(removeFirstLast("Burger"));





































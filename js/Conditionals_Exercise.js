"use strict";
/**
 * TODO 1:
 * Write some JavaScript that uses a 'confirm' dialog to ask the user if they would like to enter a number. If they click 'Ok', prompt the user for a number, then use 3 separate alerts to tell the user:
 * - Whether the number is even or odd
 * - What the number plus 100 is
 * - If the number is negative or positive
 * If the user doesn't enter a number, use an alert to tell them that, and do not display any of the information above
 **/
/**
 * TODO 2:
 * Create a function named 'color' that accepts a string that is a color name as an input. This function should return a message that related to that color. Only worry about the colors defined below, if the color passed is not one of the ones defined below, return a message that says so.
 * Ex
 * color('blue') // returns "blue is the color of the sky"
 * color('red') // returns "Roses are red"
 * You should use an if-else if- else block to return different messages.
 */
/**
 * TODO 3:
 * It's the year 2021, it's fall, and it's safe to shop again!
 * Suppose there's a promotion at Target, each customer is given a randomly generated "lucky Target number" between 0 and 5. If your lucky number is 0, you have no discount, if your lucky number is 1 you'll get a 10% discount, 2 is a 25% discount, 3 is a 35% discount, 4 is a 50% discount, and 5 you'll get a 75% discount.
 * Write a function named 'calculateTotal' that accepts a lucky number and total amount, and returns the discounted price.
 * Ex
 * calculateTotal(0, 100) // returns 100
 * calculateTotal(4, 100) // return 50
 * Test your function by passing it various values and checking for the expected return value.
 */
/**
 * TODO 4:
 * Create a grading system using if/else if/ else statements
 // BONUS Use the switch statements
 // 100 - 97 A+
 // 96 - 94 A
 // 93 - 90 A -
 // 89 - 87 B +
 // 86 - 84 B
 // 83 - 80 B -
 // 79 - 77 C +
 // 76 - 74 C
 // 73 - 70 C -
 // 69 - 67 D +
 // 66 - 64 D
 // 63 - 60 D -
 // < 59 = F
 */

/**
 * TODO 5:
 * Create a time system (MILITARY TIME) using if/else if/else statement
 * OR switch case
 * < 12 = 'Good morning'
 * < 18 = 'Good afternoon'
 * something for 'Good evening
 **/

/**
 * TODO 6:
 * Create a season system using if/else if/else statement
 * EX. 'December - February = Winter', etc.
 **/

// -----Exercise 1 Dialog with Confirm/Prompt and Alerts--------------
// had to google Confirm no previous knowledge
// First Way of doing it

// if (confirm("Would you like to enter a number?") === true) {
//
//     var input = prompt("Enter a number: ");
//
//     //if user doesn't use a number---------
//
//     if (isNaN(input)) {
//         alert("Not a number");
//     }
//
//
//
//     else {
// // check to see if input is even or odd...
//         if (input % 2 === 0) {
//             alert(input + " is an even!");
//         } else {
//             alert(input + " is an odd!");
//         }
//         //takes input and adds 100 to it...
//         alert(input + " + 100 is " + (Number(input) + 100));
//
//
//         //checks to see if input is negative or positive
//         if (input >= 0) {
//             alert(input + " is a positive number");
//         } else {
//             alert(input + " is a negative number");
//         }
//     }
//
// }
// else {
//     alert("okay, good-bye");
// }










//-----Exercise 2 Function Named Color------------------

//     function color(input) {
//         if (input === "blue") {
//             alert("Blue is the color of the sky")
//         }
//
//         else if (input === "red") {
//             alert("Roses are red");
//         }
//         else {
//             alert("That is a unique color!");
//         }
//
//     }
// //call a function
// //color("Blue")
// //
// color("yellow")


//-------Exercise 3 Promotion at Target---------------
// the function has Two parameters
// how to separate two parameters (,)
// function calculatedTotal(luckyNumber, totalAmount) {
//
//     //set conditionals next
//
//     if (luckyNumber === 0) {
//         alert(totalAmount);
//     }
//
//     else if (luckyNumber === 1) {
//         alert(totalAmount * .90);
//         // alert(totalAmount - (totalAmount * .10))
//     }
//
//     else if (luckyNumber === 2) {
//         alert(totalAmount * .75);
//     }
//
//     else if (luckyNumber === 3) {
//         alert(totalAmount * .65);
//     }
//
//     else if (luckyNumber === 4) {
//         alert(totalAmount * .50);
//     }
//
//     else if (luckyNumber === 5) {
//         alert(totalAmount * .25);
//     }
//
// }
// //call our function
//
// calculatedTotal(5, 100)


//-------Exercise 4 Grading System-------------
// google switch statement

// create an global variable
var  grade = prompt("Enter your grade: ");


    if ( grade >= 97 && grade <= 100 ) {
        alert("Grade: A+");
    }

    else if (grade >= 94 && grade <= 96 ) {
        alert("Grade A");
    }

    // --------Copy paste the rest, change grade name -----------
//     else if (grade >= 94 && grade <= 96 ) {
//         alert("Grade A");
//     }
//
//     else if (grade >= 94 && grade <= 96 ) {
//         alert("Grade A");
//     }
//
//     else if (grade >= 94 && grade <= 96 ) {
//         alert("Grade A");
//     }
// //
//
//
//
//     // last statement goes here
//
    else {
        alert("Grade: A");
    }
//
//     // switch version----Check Git Switch Statements------
//
// switch (grade) {
//         case grade
//
//
// }





//-------Exercise 5 Military Time System--------------
//


    // function time(x) {
    //
    //     if(x >= 600 && x <= 1159);
    //
    //     else if(x >= 1200 && x <=);
    //     console.log()
    //
    //     else if(x >= 1700 && x <=);
    //     console.log()
    //
    //
    // }
    //
    // time(700)








// //-----Exercise 6 Season System---------------
// var month = "September"; // or function seasons(month) { conditions go here}
//
//
//     if(month == "December" || month === "January" || month === "February" ) {
//         console.log("winter"); // or add alert(*****);
//     }
//
//     else if(month == "March" || month === "April" || month === "May") {
//         console.log("Spring"); // or add alert(***);
//     }
//
//     else if(month == "June" || month === "July" || month === "August") {
//         console.log("Summer");
//     }
//
//     else {
//         console.log("Fall");
// }
// call the function....using function seasons() above
// seasons( month: "April");  just enter any month to test




// -------------Seasons Function converted using a Switch statement------------------

// function seasonsCase(month) {
//         switch (month) {
//             case "December":
//                 alert("Winter is Here");
//                 break;
//
//             case "January":
//                 alert("Winter is Here");
//                 break;
//
//             case "February":
//                 alert("Winter is Here");
//                 break;
//
//             default:
//                 alert("What planet are you from");
//                 break;
//
//         }
// }
// seasonsCase("February");
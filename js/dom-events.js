


//event listeners

/*
Syntax:
    target.addEventListner(type=click?, listener/function,[useCapture])
    -listener: is the function that is called when an event happens on the target

 */

/*
Type of events
keyup or keydown

 */


var testButton = document.getElementById("testBtn");

//add event listener using an anonymous function aka callback function with no parameter(s)

// testButton.addEventListener("click", function () {
//     if (document.body.style.backgroundColor === "red") {
//         document.body.style.backgroundColor = "white";
//     }
//     else {
//         document.body.style.backgroundColor = "red";
//     }
//
//
// });

// event listener associated with previous defined function
// better alternative to previous

function toggleBackgroundColor() {
    if (document.body.style.backgroundColor === "red") {
        document.body.style.backgroundColor = "white";
    }
    else {
        document.body.style.backgroundColor = "red";
    }

}

testButton.addEventListener("click", toggleBackgroundColor);

///Register additional events
//when a cursor hovers a paragraph
// also, change
//target paragraph elements

// var paragraph = document.querySelector("p");
//
// //creATE
// function makeParagraphChange() {
//     paragraph.style.color = 'green';
//     paragraph.style.fontFamily = 'Comic Sans Ms';
//     paragraph.style.fontSize = "30px";
// }
// // add a mouseover event to the 'p' that will trigger the make....
// paragraph.addEventListener("mouseover", makeParagraphChange);
//only one paragraph changed because we used querySelector vs querySelectorAll




// make all elements change font and color

var paragraph = document.querySelector("p");

//creATE a function that will change the color, font-family, and font size
// use a loop, and eventListener for every element
function makeParagraphChange() {
    paragraph.style.color = 'green';
    paragraph.style.fontFamily = 'Comic Sans Ms';
    paragraph.style.fontSize = "30px";
}
// add a mouseover event to the 'p' that will trigger the make....
paragraph.addEventListener("mouseover", makeParagraphChange);

//lets target UL amd list items
//create a funtion that will change the background to blue

function changeBackgroundToBlue(e) {
    e.target.style.backgroundColor = "blue";
    //e.target is clicking/mouseover an item one at a time

}
//use for loop
var listItems = document.getElementsByTagName("li");

for (var i = 0; i < listItems.length; i++) {
    listItems[i].addEventListener("click", changeBackgroundToBlue);
    //you can change "click" to "mouseover"
}


















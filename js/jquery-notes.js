    "use strict"; // strict mode


    //jquery Document Ready
    $(document).ready(function () {
        alert("This page finished loading...")
    });

    //same as Javascript
    //window.onload = function () { alert...};

    //using id selector and attach .html method
    var content = $("#codebound").html(); // .html () property is similar to .innerHTML property in javascript
    console.log(content);

    // css selector | .css similar to .style property in javascript

    $(".urgent").css("background-color", "red"); // single property. "property" comma and then value

    $(".not-urgent").css(
        {
            "background-color": "yellow",
            "text-decoration": "line-through"
        }
    ); // "property": "value" followed by comma for multiple properties

    //multiple selector like urgent and p and #codebound
    $(".urgent, p").css("color", "orange"); // p needs to be inside a big quotation

    // All selector
    // $("*").css("background-color", "green");

    //Element selector
    $("h1").css("text-decoration", "underline");

    //inputs
    $(":button").css("color", "red");
    $(":submit").css("color", "orange");
    $(":text").css("color", "blue");



    //links
    $("[href]").css("color", "red");



















